#!/usr/bin/env python
import sys
sys.path.append("Antis")
import MP3,Office_2003,PDF
#Anyadir exif cuando no de problemas

class Clean:
	
	def __init__(self,pathFile):
		
		self.pathFile = pathFile
		self.extension = self.__extractExtension()
		self.__manageScripts()
	
	def __extractExtension(self):
		
		ext = self.pathFile[self.pathFile.rfind('.')+1:]
		return ext
	
	def __manageScripts(self):
			
		#if self.extension=="jpg":
		#	Exif.clean_EXIF(self.pathFile)
		if self.extension=="mp3":
			MP3.clean_MP3(self.pathFile)
		if self.extension in ["doc","xls","ppt"]:
			Office_2003.clean_Office2003(self.pathFile)
		if self.extension=="pdf":
			PDF.clean_PDF(self.pathFile)
		
