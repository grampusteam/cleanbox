#!/usr/bin/python
#-*- coding:utf-8 -*-

#Microsoft Office 2007
#Metadata extractor and anti extractor

#Modules - Imports
#------------------------------------------------#
from xml.dom.minidom import parse
from tempfile import mkdtemp
from shutil import rmtree, copyfileobj, copyfile
from zipfile import ZipFile, is_zipfile
import os, ms_EXIF, sys
import anti_ms_Exif
#-------------------------------------------------#


#Extractor - Beginning
# ------------------- #
class ms2007():
    def __init__(self):
        self.Date = {"01": "January",
        "02": "February",
        "03": "March",
        "04": "April",
        "05": "May",
        "06": "June",
        "07": "July",
        "08": "August",
        "09": "September",
        "10": "October",
        "11": "November",
        "12": "December"}

        self.metaDatos = {"Title": "",
        "Users": [],
        "Date": {"Created": "", "Modified": "", "Printed": ""},
        "Aplicacion": "",
        "Business": "",
        "Edited": ""}

    def extract(self, archivo):
        self.archivo = archivo
        self.ruta = mkdtemp(prefix='gra')
        if self.__descomprimir():
            self.__docProps()
            self.__extraData()
        else:
            return {"Error": "not is a valid file"}
        rmtree(self.ruta)
        return self.metaDatos
        

    def _img_extract(self):
        self.__img_uncompress()
        self._img_meta_extractor()

    def __descomprimir(self):
        if not is_zipfile(self.archivo):
            return False
        buff = ZipFile(self.archivo, 'r')
        for i in buff.namelist():
            if i in ('docProps/core.xml', 'docProps/app.xml',
                'word/document.xml', 'word/_rels/document.xml.rels'):
                filename = os.path.basename(i)
                source = buff.open(i)
                target = file(os.path.join(self.ruta, filename), 'wb')
                copyfileobj(source, target)
                source.close()
                target.close()
        return True

    def __docProps(self):
        try:
            core = parse(os.path.join(self.ruta, 'core.xml'))
            app = parse(os.path.join(self.ruta, 'app.xml'))
        except:
            print "Can't parse core/app .xml"

        try:
            self.metaDatos["Users"].append(
            self.__getMetaData(core, "dc:creator"))

            self.metaDatos["Title"] = \
            self.__getMetaData(core, "dc:title")

            self.metaDatos["Users"].append(
            self.__getMetaData(core, "cp:lastModifiedBy"))

            self.metaDatos["Aplicacion"] = self.__getMetaData(app, "Application")
            self.metaDatos["Business"] = self.__getMetaData(app, "Company")
            self.metaDatos["Edited"] = \
            self.__getMetaData(core, "cp:revision") + " times"

            self.metaDatos["Date"]["Created"] = \
            self.__W3CDTF(self.__getMetaData(core, "dcterms:created"))

            self.metaDatos["Date"]["Modified"] = \
            self.__W3CDTF(self.__getMetaData(core, "dcterms:modified"))

            self.metaDatos["Date"]["Printed"] = \
            self.__W3CDTF(self.__getMetaData(core, "cp:lastPrinted"))
        except:
            print "ERROR, can't extract the metadata from the document\n"

    def __W3CDTF(self, Date):
        if Date is None:
            return "Never"
        return self.Date[Date[5:7]] + str(Date[5:7] + ', ' + Date[0:4] + ' at: ' + Date[14:19])

    def __getMetaData(self, xml, tag):
        buff = xml.getElementsByTagName(tag)
        if len(buff):
            if not buff[0].firstChild is None:
                return buff[0].firstChild.toxml()
            else:
                return None

    def __extraData(self):
        self.metaDatos["Links"] = []
        try:
            doc = os.path.join(self.ruta, 'document.xml')
            rel = os.path.join(self.ruta, 'document.xml.rels')
        except:
            print "Cant parse document.xml and document.xml.rels"
            
        if os.path.isfile(doc) and os.path.isfile(rel):
            document = parse(doc)
            links = parse(rel)

            buff = document.getElementsByTagName('w:ins')
            if len(buff):
                for i in buff:
                    usuario = i.getAttribute('w:author')
                    if not usuario in self.metaDatos["Users"]:
                        self.metaDatos["Users"].append(usuario)

            buff = links.getElementsByTagName('Relationship')
            if len(buff):
                for i in buff:
                    if i.getAttribute('TargetMode'):
                        link = i.getAttribute('Target')
                        if not link in self.metaDatos["Links"]:
                            self.metaDatos["Links"].append(link)

    #adding another functions to extract images from office2007 documents and show the exif metadata contained into the images

    def __img_uncompress(self):
        buff = ZipFile(self.archivo, 'r')
        for name in buff.namelist():
            #we must to add another extensions
            if (name.find('.jpeg')!= -1):
                buff.extract(name)

    def _img_meta_extractor(self):
        var = self.archivo[self.archivo.rfind('.'):]
        if var == ".docx":
            images = os.listdir('word/media/')
            counter = 0
            for i in images:
                counter = counter+1
                try:
                    os.rename('word/media/%s'%i, "word/media/image%s.jpg"%(counter))
                except:
                    print "an error has ocurred renaming the images"
                    continue
            images = os.listdir('word/media/')
            for x in images:
                obj = ms_EXIF.ms_EXIF('word/media/%s'% x)
                print "\n - - - - - - - - - - - - - - \n"
                print "%s metadata : "%x
                print "----------------------------"
                print obj.returnMetaData()
            rmtree('word/')

        elif var == ".pptx":
            images = os.listdir('ppt/media/')
            counter = 0
            for i in images:
                counter = counter+1
                try:
                    os.rename('ppt/media/%s'%i, "ppt/media/image%s.jpg"%(counter))
                except:
                    print "an error has ocurred renaming the images"
                    continue
            images = os.listdir('ppt/media/')
            for x in images:
                obj = ms_EXIF.ms_EXIF('ppt/media/%s'% x)
                print "\n - - - - - - - - - - - - - - \n"
                print "%s metadata : "%x
                print "----------------------------"
                print obj.returnMetaData()
            rmtree('ppt/')

        elif var == ".xlsx":
            images = os.listdir('xl/media/')
            counter = 0
            for i in images:
                counter = counter+1
                try:
                    os.rename('xl/media/%s'%i, "xl/media/image%s.jpg"%(counter))
                except:
                    print "an error has ocurred renaming the images"
                    continue
            images = os.listdir('xl/media/')
            for x in images:
                obj = ms_EXIF.ms_EXIF('xl/media/%s'% x)
                print "\n - - - - - - - - - - - - - - \n"
                print "%s metadata : "%x
                print "----------------------------"
                print obj.returnMetaData()
            rmtree('xl/')

        else:
            print "ERROR"


obj = ms2007()
print obj.extract('test.docx')
obj._img_extract()
# ------------------- #
#Extractor - End


#Anti Extractor - Beginning
# ------------------- #
class Anti_office2007():

    def __init__(self):
        self._ms_do('test.docx', 'nuevo.docx')

    def _ms_do(self, sDocName, newDocName):
        self.sDocName = sDocName
        self.newDocName = newDocName

        if self.__uncompress():
            self._xml_cleaner()
            self.__compress()
            self._meta_adder()
            self._image_manag()
        else:
            print "error"
            sys.exit(0)

    def __uncompress(self):
    	#Uncompressing core.xml and app.xml to edit metadata
        if not zipfile.is_zipfile(self.sDocName):
            return False
        buff = zipfile.ZipFile(self.sDocName, 'r')
        for i in buff.namelist():
            if i in ('docProps/core.xml', 'docProps/app.xml'):
                filename = os.path.basename(i)
                source = buff.open(i)
                target = file(os.path.join(filename), 'wb')
                copyfileobj(source, target)
                source.close()
                target.close()
        return True

    def _xml_cleaner(self):
    	#parsing core.xml to replace the values
        core = parse(os.path.join('core.xml'))
        corelist = ['dc:creator', 'dc:title', 'cp:lastModifiedBy', 'cp:revision',
                    'dcterms:created', 'dcterms:modified', 'cp:lastPrinted']
        for i in corelist:
            try:
                core.getElementsByTagName(i)[0].childNodes[0].nodeValue = ""
            except:
                continue
        #saving
        f = open(os.path.join('core.xml'), 'w')
        core.writexml(f)
        f.close()
        #parsing app.xml to replace values
        app = parse(os.path.join('app.xml'))
        applist = ['Application', 'Company']

        for x in applist:
            try:
                app.getElementsByTagName(x)[0].childNodes[0].nodeValue = ""
            except:
                continue
        #saving
        j = open(os.path.join('app.xml'), 'w')
        app.writexml(j)
        j.close()

    def __compress(self):
    	#creating the new doc
        zf = zipfile.ZipFile(self.sDocName, 'r')
        zp = zipfile.ZipFile(self.newDocName, 'w')
        try:
            for item in zf.infolist():
                buffer = zf.read(item.filename)
                #core and app .xml will be joined later in meta_adder func
                if (item.filename[-8:] != 'core.xml') and (item.filename[-7:] != 'app.xml') and (item.filename[-5:] != '.jpeg'):
                    zp.writestr(item, buffer)
            zf.close()
            zp.close()
        except:
            print "compressing error"
            sys.exit(0)

    def _meta_adder(self):
    	#joining core and app.xml
        zf = zipfile.ZipFile(self.newDocName, 'a')
        try:
        	zf.write('core.xml')
       		zf.write('app.xml')
        	zf.close()
        except:
        	print "error in writting"
        	sys.exit(0)
        #removing core and app .xml because it's already joined
        os.remove('core.xml')
        os.remove('app.xml')


#adding another functions to clean exif metadata from the images where are into the 2007 office documents

    def __img_uncompress(self):
        buff = zipfile.ZipFile(self.sDocName, 'r')
        for name in buff.namelist():
            #we must to add another extensions
            if (name.find('.jpeg')!= -1):
                buff.extract(name)

    def _img_meta_extractor(self):
        images = os.listdir('word/media/')
        counter = 0
        for i in images:
            counter = counter+1
            try:
                os.rename('word/media/%s'%(i), "word/media/image%s.jpg"%(counter))
            except:
                print "an error has ocurred renaming the images"
                continue

        images = os.listdir('word/media/')
        var = 0
        while (var<3):
            for x in images:
                obj = anti_ms_Exif.anti_ms_Exif('word/media/%s'% x)
            var = var+1

    def _adder(self):
        #before to add the cleaned images into the document
        #we must to change the ext again
        images = os.listdir('word/media/')
        counter = 0
        for i in images:
            counter = counter+1
            try:
                os.rename('word/media/%s'%(i), "word/media/image%s.jpeg"%(counter))
            except:
                print "an error has ocurred renaming the images"
                continue
        #joining cleaned images into the newDocName
        zf = zipfile.ZipFile(self.newDocName, 'a')
        images = os.listdir('word/media/')
        for x in images:
            try:
                print "Writing %s in newdoc:"%x
                zf.write('word/media/%s'% x)
            except:
                print "error in Writing"
                sys.exit(0)
        zf.close()
        rmtree('word/')

    def _image_manag(self):
        self.__img_uncompress()
        self._img_meta_extractor()
        self._adder()

obj = Anti_office2007()
# ------------------- #
#Anti Extractor - End