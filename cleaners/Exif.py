import pyexiv2,sys
class extract_EXIF:
	
	def __init__(self,pathFile):
		
		self.pathFile = pathFile
		self.metaDictionary = {}
		self.__openFile()
		self.__extractMetadata()
	
	# Open File To Extract Metadata
	def __openFile(self):
		
		try:
			self.metaData = pyexiv2.ImageMetadata(self.pathFile)
		except:
			print "File not exist"
			sys.exit(0)
	
	# Extract Metadata From Image With EXIF Especification
	def __extractMetadata(self):
		
		try:
			self.metaData.read()
			for self.data in self.metaData.exif_keys:
				self.metaDictionary[(self.metaData.__getitem__(self.data).key.replace("Exif.",""))] = (self.metaData.__getitem__(self.data)).value
		except:
			print "Information not available"
			sys.exit(0)
			
	#Show Extracted Metadata From Image
	def _extract(self):
		
		return self.metaDictionary
			
class clean_EXIF:
	
	def __init__(self,pathFile):
		
		self.pathFile = pathFile
		self.__replaceMetaData()
	
	def __replaceMetaData(self):
		
		metaData = pyexiv2.metadata.ImageMetadata(self.pathFile)
		metaData.read()
		countKey = 0
		while countKey<=len(metaData.exif_keys):
			for keys in metaData.exif_keys:
				try:
					metaData.__delitem__(keys)
				except:
					continue
			metaData.write()
			countKey += 1
