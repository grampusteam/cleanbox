#!/usr/bin/env python
import pyexiv2

class anti_ms_Exif:
	
	def __init__(self,pathFile):
		
		self.pathFile = pathFile
		self.__replaceMetaData()
	
	# Currently replaces metadata, will be studied in future versions if another implementation is more efficient
	def __replaceMetaData(self):
		
		metaData = pyexiv2.metadata.ImageMetadata(self.pathFile)
		metaData.read()
		for key in metaData.exif_keys:
			try:
				metaData.__setitem__(key,"")
				metaData.write()
			except:
				continue

if __name__ == "__main__":
	anti_ms_Exif("imagen.jpg")
