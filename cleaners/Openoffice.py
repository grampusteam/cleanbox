#!/usr/bin/python
#-*- coding:utf-8 -*-

#OpenOffice Extractor
# 		&
#OpenOffice Anti Extractor

#Modules - Imports
#-----------------------------------------#
from xml.dom.minidom import parse
from shutil import copyfileobj, rmtree
from tempfile import mkdtemp
import zipfile, os, sys, ms_EXIF, anti_ms_Exif
#-----------------------------------------#

#--------------------------#
#Extractor - Beginning
#--------------------------#
class ms_OpenOffice():

    def __init__(self):
        self.metaData = {"Date": {},
        "Links": [],
        "Mails": []}

    def _ms_do(self, archivo):
        self.archivo = archivo
        self.ruta = mkdtemp(prefix='gra')
        if self.__uncompress():
            self._ms_xml_parser()
            self._ms_extraxml_parser()
            return self.metaData
        else:
            return {"Error": "not is a valid file"}

    def __uncompress(self):
        if not zipfile.is_zipfile(self.archivo):
            return False
        buff = zipfile.ZipFile(self.archivo, 'r')
        for i in buff.namelist():
            if i in ('meta.xml', 'content.xml', 'settings.xml'):
                filename = os.path.basename(i)
                source = buff.open(i)
                target = file(os.path.join(self.ruta, filename), 'wb')
                copyfileobj(source, target)
                source.close()
                target.close()
        return True

    def _ms_xml_parser(self):
        try:
            core = parse(os.path.join(self.ruta, 'meta.xml'))
        except:
            print "Can't parse meta.xml"
        try:
            self.metaData["Date"]["Creation"] =\
            self.__getMetaData(core, "meta:creation-date")
            self.metaData["Date"]["Modification"] =\
            self.__getMetaData(core, "dc:date")
            self.metaData["Date"]["Modification Times"] =\
            self.__getMetaData(core, "meta:editing-cycles")
            self.metaData["Aplication"] =\
            self.__getMetaData(core, "meta:generator")
            self.metaData["Title"] = self.__getMetaData(core, "dc:title")
            self.metaData["Description"] =\
            self.__getMetaData(core, "dc:description")
            self.metaData["Keywords"] = self.__getMetaData(core, "meta:keyword")
            self.metaData["Languaje"] = self.__getMetaData(core, "dc:language")
            self.metaData["User"] =\
            self.__getMetaData(core, "meta:initial-creator")
        except:
            print "Can't extract metadata from the document"

    def _ms_extraxml_parser(self):
        content = parse(os.path.join(self.ruta, 'content.xml'))
        settings = parse(os.path.join(self.ruta, 'settings.xml'))
        buff = content.getElementsByTagName('text:a')
        if len(buff):
            for i in buff:
                if i.getAttribute('xlink:href'):
                    link = i.getAttribute('xlink:href')
                    if link[0:7] == 'mailto:':
                        if not link in self.metaData["Mails"]:
                            self.metaData["Mails"].append(link[7:])
                    else:
                        if not link in self.metaData["Links"]:
                            self.metaData["Links"].append(link)

            buff = settings.getElementsByTagName('config:config-item')
            if len(buff):
                for i in buff:
                    if i.getAttribute('config:name') == 'PrinterName':
                        if not i.firstChild is None:
                            self.metaData["Printer"] = i.firstChild.toxml()

    def __getMetaData(self, xml, tag):
        buff = xml.getElementsByTagName(tag)
        if len(buff):
            if not buff[0].firstChild is None:
                return buff[0].firstChild.toxml()
            else:
                return None

    #this func need a little fix
    """def __version(self, data):
        data = re.findall('(.*)\.org/(.*)\$(.*) (.*)/(.*).*', data)
        self.metaData["SO"] = data[0][2]
        return data[0][0] + " " + data[0][1]"""


#adding another functions to extract images from office2007 documents and show the exif metadata contained into the images

    def _img_extract(self):
        self.__img_uncompress()
        self._img_meta_extractor()

    def __img_uncompress(self):
        buff = zipfile.ZipFile(self.archivo, 'r')
        for name in buff.namelist():
            #we must to add another extensions
            if (name.find('.jpg')!= -1):
                buff.extract(name)

    def _img_meta_extractor(self):
        try:
            images = os.listdir('Pictures/')
            for x in images:
                obj = ms_EXIF.ms_EXIF('Pictures/%s'% x)
                print "\n - - - - - - - - - - - - - - \n"
                print "%s metadata : "%x
                print "----------------------------"
                print obj.returnMetaData()
            rmtree('Pictures/')
        except:
            print "ERROR extracting images"

obj = ms_OpenOffice()
print obj._ms_do('test.odt')
obj._img_extract()
#---------------------#
#Extractor - End
#---------------------#

#--------------------------#
#Anti Extractor - Beginning
#--------------------------#
class Anti_OpenOffice():

    def __init__(self):
        self._ms_do('test.odt', 'new_test.odt')

    def _ms_do(self, sDocName, newDocName):
        self.sDocName = sDocName
        self.newDocName = newDocName

        #uncompressing
        if self.__uncompress():
            #cleaning xml files
            self._xml_cleaner()
            self._xml_extra_cleaner()
            #compressing , adding and deleting
            self.__compress()
            self._meta_adder()
            self._image_manag()
        else:
            print "An error has ocurred uncompressing"
            sys.exit(0)


    def __uncompress(self):
        #uncompressing metadata containers
        if not zipfile.is_zipfile(self.sDocName):
            return False
        buff = zipfile.ZipFile(self.sDocName, 'r')
        for i in buff.namelist():
            if i in ('meta.xml', 'content.xml', 'settings.xml'):
                filename = os.path.basename(i)
                source = buff.open(i)
                target = file(os.path.join(filename), 'wb')
                copyfileobj(source, target)
                source.close()
                target.close()
        return True

    def _xml_cleaner(self):
        dom = parse(os.path.join('meta.xml'))
        metalist = ['meta:creation-date',
                    'dc:date',
                    'meta:editing-cycles',
                    'meta:editing-duration',
                    'meta:generator',
                    'dc:title',
                    'dc:description',
                    'meta:keyword',
                    'dc:language',
                    'meta:initial-creator',
                    'dc:creator']

        #cleaning tags values
        for i in metalist:
            try:
                for a in dom.getElementsByTagName(i):
                    a.childNodes[0].nodeValue = ""
            except:
                print "Error, tagname not found"
                sys.exit(0)
        #Saving in meta.xml
        f = open(os.path.join('meta.xml'), 'w')
        dom.writexml(f)
        f.close()

    def _xml_extra_cleaner(self):
        #cleaning tags values in content.xml
        content = parse(os.path.join('content.xml'))
        content_tag = content.getElementsByTagName("text:a")
        for node in content_tag:
            try:
                node.setAttribute('xlink:href', str(''))
            except:
                print "Error, tagname not found"
                sys.exit(0)

        f = open(os.path.join('content.xml'), 'wb')
        content.writexml(f)
        f.close()

        #cleaning tags values in settings.xml(WILL MUST CORRECT IT)
        #PENDING A FIX FOR IT(when we have more time)
        """
        settings = parse(os.path.join('settings.xml'))
        settings_tag = settings.getElementsByTagName("config:config-item")
        for another_node in settings_tag:
            try:
               another_node.setAttribute('config:name', str(''))
            except:
               print "An error has ocurred, but not is very important, you can continue"

        j = open(os.path.join('settings.xml'), 'w')
        settings.writexml(j)
        j.close()
        """

    def __compress(self):
        zf = zipfile.ZipFile(self.sDocName, 'r')
        zp = zipfile.ZipFile(self.newDocName, 'w')

        for item in zf.infolist():
            try:
                #triying to write a new document without meta,content & settings .xml
                buffer = zf.read(item.filename)
                if (item.filename[-8:] != 'meta.xml') and (item.filename[-11:] != 'content.xml') and (item.filename[-12:] != 'settings.xml'):
                    zp.writestr(item, buffer)
            except:
                print "Can't write"
                sys.exit(0)

        zf.close()
        zp.close()

    def _meta_adder(self):
        zf = zipfile.ZipFile(self.newDocName, 'a')
        zf.write('meta.xml')
        zf.write('content.xml')
        zf.write('settings.xml')
        zf.close()
        #deleting container files
        os.remove('meta.xml')
        os.remove('content.xml')
        os.remove('settings.xml')

#adding another functions to clean exif metadata from the images where are into openoffice documents    

    def __img_uncompress(self):
        buff = zipfile.ZipFile(self.sDocName, 'r')
        for name in buff.namelist():
            #we must to add another extensions
            if (name.find('.jpeg')!= -1):
                buff.extract(name)


    def _img_meta_extractor(self):
        images = os.listdir('Pictures/')
        var = 0
        while (var<3):
            for x in images:
                obj = anti_ms_Exif.anti_ms_Exif('Pictures/%s'% x)
            var = var+1        


    def _adder(self):
        zf = zipfile.ZipFile(self.newDocName, 'a')
        images = os.listdir('Pictures/')
        for x in images:
            try:
                print "Writing %s in newdoc:"%x
                zf.write('Pictures/%s'%x)
            except:
                print "Error in writing"
                sys.exit(0)
        zf.close()
        rmtree('Pictures/')

    def _image_manag(self):
        self.__img_uncompress()
        self._img_meta_extractor()
        self._adder()        

obj = Anti_OpenOffice()
#---------------------#
#Anti Extractor - End
#---------------------#