#!/usr/bin/env python
import Log,Options,Clean
import os
class CleanHex:

	def __init__(self):

		self.dropLogin = Log.Log()
		self.setOptions = Options.Options(self.dropLogin._returnClient())
		self.__manageCleanAll(".pdf")		

	def __manageCleanAll(self,fileExtension,pathWfolder=""):
		# Download all files
		listOfFiles = self.setOptions._downloadAllFiles(pathWfolder,fileExtension)
		# Clean, upload and extract url from new and cleaned file uploaded
		self.__manageUpload(listOfFiles)
		self.__cleanDownloads(listOfFiles)
		
	def __manageUpload(self,listOfFiles,pathWfolder=""):
		if pathWfolder=="":
			#Si no se le pasa parametro sube los archivos a la raiz
			for fileExt in listOfFiles:
				self.__cleanFile(fileExt)
				self.__uploadFile(fileExt)
				self.__extractUrl(fileExt)
		else:
			#Si se le pasa crea el folder y sube los archivos dentro
			dropFolder = self.setOptions._createFolder(pathWfolder)
			for fileExt in listOfFiles:
				self.__cleanFile(fileExt)
				self.__uploadFile(fileExt,dropFolder+"/"+fileExt)
				self.__extractUrl(dropFolder+"/"+fileExt)

	# Este es el que sube el fichero a dropbox llamando a la funcion de subida que ofrece Options.
	# Si no se le pasa el parametro folder se suben
	def __uploadFile(self,pathFile,pathWfolder=""):
		try:
			if pathWfolder!="":
				self.setOptions._uploadFile(pathFile,pathWfolder)
			else:
				self.setOptions._uploadFile(pathFile)
		except:
			print "Problems Uploading :("

	def __extractUrl(self,pathFile):
		print self.setOptions._shareLinkFile(pathFile)
	
	def __cleanDownloads(self,listOfFiles):
		
		for pathFile in os.listdir("."):
			if pathFile in listOfFiles:
				os.remove(pathFile)
			

	def __cleanFile(self,pathFile):
		try:
			Clean.Clean(pathFile)
		except:
			print "Problems Cleaning :("
		else:
			print "Cleaned :)"

CleanHex()
