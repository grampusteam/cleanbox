#!/usr/bin/env python
class Options:
	
	# Client is returned by method _returnClient in log_Drop
	def __init__(self,client):
		self.client = client
	
	def _uploadFile(self,pathFile,pathWithfolder=""):
		if pathWithfolder!="":
			file2Upload = open(pathFile,'rb')
			response = self.client.put_file(pathWithfolder,file2Upload)
			file2Upload.close()
		else:
			file2Upload = open(pathFile,'rb')
			response = self.client.put_file(pathFile,file2Upload)
			file2Upload.close()
	
	# Devuelve una lista con los archivos que se ha bajado, para posteriormente usarla en cleanAllFiles y limpiarlos para volver a subirlos
	def _downloadAllFiles(self,dirb,ext):
		dirInfo = self.client.search(dirb,ext)
		listOfFiles = []
		for metaData in dirInfo:
			listOfFiles.append(metaData["path"][1:])
			openFile = open(metaData["path"][1:],"wb")
			openFile.write(self._getFile(metaData["path"][1:]))
			openFile.close()
			self.client.file_delete(metaData["path"][1:])
		return listOfFiles
			
	def _createFolder(self,pathFile):
		return self.client.file_create_folder(pathFile)["path"]
	 
	def _deleteFile(self,pathFile):
		self.client.file_delete(pathFile)
	
	def _getFile(self,pathFile):
		return self.client.get_file(pathFile).read()
	
	def _shareLinkFile(self,pathFile):
		return self.client.share(pathFile)["url"]
	
	def _fileCopy(self,pathFile,newPath):
		self.client.copy_file(pathFile,newPath)
	
	# Posible implementacion premium
	def _accountInfo(self):
		return self.client.account_info()
		
		

